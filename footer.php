<div id="player">
	

	<div id="footer" class="container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<div class="row">
		
			<div class="playerControls col-xs-1 col-sm-1 col-md-1 col-lg-1">

			<div class="button-space">
					<button class="glyphicon glyphicon-step-backward" title="Previous"
						onclick="previousTrack();"></button>
			</div>
			
			<div class="button-space">
					<button id="audioPlay" class="glyphicon glyphicon-play" title="Play"
						onclick="playPause();"></button>
					<button id="audioPause" class="hidden" title="Pause"
						onclick="playPause();"></button>
			</div>
		

			<div class="button-space">
					<button class="glyphicon glyphicon-step-forward" title="Next"
						onclick="nextTrack();"></button>
			</div>

			<!--<div class="button-space">
					<button class="stop" id="audioStop" title="Stop"
						onclick="playStop();"></button>
			</div>-->

			</div>
		

			<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
					<div id="audioElapsed">
					</div>
				</div>

			<div class="playerSeek col-xs-8 col-sm-8 col-md-8 col-lg-8" id="playerSeek">
				<div id="audioSeek">
					<div id="audioLoaded">
					</div>
				</div>
			
			</div>
					<!--<li id="audioDuration">00:00:00</li>-->
		</div>
	</div>
</div>

<script>

var player = document.getElementById('player');
var audio = document.createElement('audio');

audio.addEventListener("timeupdate", setElapsed);

function playTrack(url) {
	var track = audioSourcesList.indexOf(url);
	if(track != -1) {
		audio.src = audioSourcesList[track];
		audio.id = url;
		player.appendChild(audio);
	}
}

var audioElapsed = document.getElementById("audioElapsed");
var audioPlay = document.getElementById("audioPlay");
var audioPause = document.getElementById("audioPause");
var audioStop = document.getElementById("audioStop");
var audioLoaded = document.getElementById("audioLoaded");
//audio.addEventListener("loadedmetadata", setDuration, false);

var playerSeekWidth = document.getElementById("playerSeek").offsetWidth;
var audioSeek = document.getElementById("audioSeek");

audioSeek.style.width = String(playerSeekWidth);

var clicking = false;

$('#audioSeek').mousedown(function () {
	clicking = true;
});

$(document).mouseup(function () {
	clicking = false;
});

$('#audioSeek').mousemove(function(e) {
	if(clicking) { 
        var posX = $(this).position().left;
        var posY = $(this).position().top;
        console.log("mousemove", posX, posY, e.pageX, e.pageY, (e.pageX - posX));
        audioSeekWidth((parseFloat(e.pageX) - posX));
    }
});

function audioSeekWidth(position) {
	audio.currentTime = audio.duration / audioSeek.offsetWidth * position;
	console.log("audioSeekWidth", audio.duration, audioSeek.offsetWidth, position);
}

function setDuration(event) {
	audioDuration.innerHTML = timeFormatter(audio.duration);
}

function setElapsed(event) {
	audioElapsed.innerHTML = "-" + timeFormatter(audio.duration - audio.currentTime);
	amountLoaded = (audio.currentTime/audio.duration)*audioSeek.offsetWidth;
	audioLoaded.style.width = amountLoaded + 'px';
}

function playPause() {
	var nowPlaying = document.getElementById(audio.id);

	if (audio.paused){
	audio.play();
	audioPlay.className = 'hidden';
	audioPause.className = 'glyphicon glyphicon-pause';
	nowPlaying.className = 'glyphicon glyphicon-pause';

	} else if (!audio.paused){
	audio.pause();
	audioPlay.className = 'glyphicon glyphicon-play';
	audioPause.className = 'hidden';
	nowPlaying.className = 'glyphicon glyphicon-play';
	}
}

/*function playStop() {
	audio.pause();
	audio.currentTime=0;
	audioPlay.className = 'glyphicon glyphicon-play';
	audioPause.className = 'hidden';
}*/

function nextTrack() {
	var sources = audioSourcesList;
	var currentTrack = sources.indexOf(document.getElementById('nowplaying').src);
	var i = currentTrack + 1;
	console.log(currentTrack, i);
	if(sources.length > i > -1) {
		audio.pause();
		audio.src = sources[i];
		audio.play();
	} else if(sources.length <= i || i <= -1) {
		audio.src = sources[0];
		audio.play();
	}
}

function previousTrack() {
	var sources = audioSourcesList;
	var currentTrack = sources.indexOf(document.getElementById('nowplaying').src);
	var i = currentTrack - 1;
	console.log(currentTrack, i);
	if(sources.length > i > -1) {
		audio.pause();
		audio.src = sources[i];
		audio.play();
	}
}

function timeFormatter(seconds){
	function zeroPad(str) {
		if (str.length > 2) return str;
		for (i=0; i<(2-str.length); i++) {
			str = "0" + str;
		}
		return str;
	}
	var minute = 60,
	hour = minute * 60,
	hStr = "",
	mStr = "",
	sStr = "";

	var h = Math.floor(seconds / hour);
	hStr = zeroPad(String(h));

	var m = Math.floor((seconds - (h * hour)) / minute);
	mStr = zeroPad(String(m));

	var s = Math.floor((seconds - (h * hour)) - (m * minute));
	sStr = zeroPad(String(s));
	return (hStr + ":" + mStr + ":" + sStr);
}
</script>

<style>
:before {
	color: white;
	display: block;
}

#footer {
	z-index: 1000;
    position: fixed;
    height: 50px;
    background-color: #000;
    bottom: 0px;
    left: 0px;
    right: 0px;
    margin-bottom: 0px;
}

body {
    margin-bottom:50px;
    height: 100%;
}

.playerControls {
	display: flex;
	top: 15px;
}

.pause {
	background: red;
	width: 5px;
	height: 20px;
	position: relative;
}

.pause::after {
    background: red;
    content: "";
    height: 20px;
    width: 5px;
    margin-left: 5px;
    bottom: 0px;
    position: absolute;
}

.stop {
	width: 20px;
	height: 20px;
	background: red;
}


.play {
	width: 0;
	height: 0;
	border-top: 10px solid transparent;
	border-left: 20px solid red;
	border-bottom: 10px solid transparent;
}

.next {
	width: 0;
	height: 0;
	bottom: -10px;
	border-top: 10px solid transparent;
	border-left: 20px solid red;
	border-bottom: 10px solid transparent;
}

.previous {
	width: 0;
	height: 0;
	bottom: -10px;
	border-top: 10px solid transparent;
	border-right: 20px solid red;
	border-bottom: 10px solid transparent;
}

.next::after {
    background: red;
    content: "";
    height: 20px;
    width: 5px;
    bottom: 0px;
    position: absolute;
}

.previous::before {
    background: red;
    content: "";
    height: 20px;
    width: 5px;
    left: 10px;
    bottom: 0px;
    position: absolute;
}

.playerSeek {
	display: flex;
	top: 35px;
}

.playerHandle {
	border: 1px solid #f50;
    border-radius: 100%;
    height: 8px;
    width: 8px;
    background-color: #f50;
    box-sizing: border-box;
    margin-top: -4px;
    margin-left: -4px;
    opacity: 0;
    transition: opacity 150ms;
}

.button-space {
	margin-right: 10px;
}

button {
	background: RGBA(0,0,0,0);
	border: 10px;
	-moz-border-radius: 5px;
	border-radius: 0px;
	bottom: 0px;
	padding: 0px;
	/*position: absolute;*/
	/*width: px; */
}

#audioStop {
font-size: 22px;
left: 65px;
line-height: 11px;
}

#audioPlay.hidden,
#audioPause.hidden { display:none; }
#audioSeek {
background: #E7E7E7;
padding: 0;
/*display: block;*/
height: 5px;
}

#audioLoaded {
background: white;
display: block;
top: 1px;
position: relative;
height: 3px;
}

#audioElapsed {
	position: relative;
	top: 10px;
}
</style>

</body>
</html>
