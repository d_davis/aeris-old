<?php get_header(); ?>

<div class='container'>
	<div class='row' id='content'></div>
</div>

<script>
window.onload = function () {
  mainSwitch();
  setupHistoryClicks();
  window.addEventListener("resize", function(){
    setImageSize();
  });
  window.setTimeout(function() {
    window.addEventListener("popstate", function(e) {
      mainSwitch();
      }, false);
  }, 1);
}

var audioSourcesList = [];

function setupHistoryClicks() {
  addClicker(document.getElementById("blog"));
  addClicker(document.getElementById("albums"));
  addClicker(document.getElementById("artists"));
}

function addClicker(link) {
  console.log(link);
    link.addEventListener("click", function(e) {
    history.pushState(null, null, link.href);
    mainSwitch();
    e.preventDefault();
  }, false);
}

function checkAddressByRegExp() {
  var postLocationRegExp = /[#][/]\bblog[/][0-9]+[/][a-zA-Z0-9-_'ʹ]+/g;
  var postLocationCheck = postLocationRegExp.test(location.hash);

  var albumLocationRegExp = /[#][/]\balbums[/][a-zA-Z0-9-_'ʹ]+[/][a-zA-Z0-9-_'ʹ]+/g;
  var albumLocationCheck = albumLocationRegExp.test(location.hash);

  var artistLocationRegExp = /[#][/]\bartists[/][a-zA-Z0-9-_'ʹ]+/g;
  var artistLocationCheck = artistLocationRegExp.test(location.hash);

  if (postLocationCheck) {
    return 1;
  } else if(albumLocationCheck) {
    return 2;
  } else if (artistLocationCheck) {
    return 3;
  } else {
    return 0;
  }
}

function removeContent() {
  var content = document.getElementById("content");

  while (content.firstChild) {
      content.removeChild(content.firstChild);
  }
}

function mainSwitch() {

  if (checkAddressByRegExp() != 0) {
    useHash();
  } else {
    switch (location.hash) {
      case '#/blog':
        removeContent();
        getPosts();
        break;

      case '#/albums':
        removeContent();
        albumList();
        break;


      case '#/artists':
        removeContent();
        artistList();
        break;

      case '':
        history.pushState(null, null, '#/blog');
        removeContent();
        getPosts();
        break;

      default:
        removeContent();
        error404();
    }
  }
}

function useHash() {
  var currentPageHash = location.hash;

  switch (checkAddressByRegExp()) {
    case 1:
      console.log('blog');
      var forLink = currentPageHash.split('#/blog/');
      var link = forLink[1];
      getPostContent(link);
      break;

    case 2:
      console.log('albums');
      var forLink = currentPageHash.split('#/albums/');
      var link = forLink[1];
      removeContent();
      getAlbumContent(link);
      break;

    case 3:
      var forLink = currentPageHash.split('#/artists/');
      var link = forLink[1];
      removeContent();
      loadArtistPage(link);     
  }
  
}

//error 404 page
function error404() {
  var content = document.getElementById('content');

  var error = document.createElement('div');
  error.innerHTML = '<p style="z-index: 1000; font-family: Evolventa; font-size: 500px; text-align: center; position: relative; margin-bottom: -175px;">404</p><p style="z-index: 1000; font-family: Evolventa; font-size: 50px; text-align: center; position: relative;">What are you looking for?</p>';

  content.appendChild(error);

}

//posts page
function getPosts() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var answer = JSON.parse(xhttp.responseText);
      console.log(answer.length);

      var mainContainer = document.getElementById('content');

      var jumbotron = document.createElement("div");
      jumbotron.className = "jumbotron";
      jumbotron.innerHTML = '<video autoplay loop><source src="<?php echo get_theme_root_uri() . '/aeris/Untitled.mp4' ?>" type="video/mp4" /><source src="<?php echo get_theme_root_uri() . '/aeris/Winter-Grass.webm' ?>" type="video/webm" /></video>';/*<div class="poster hidden"><img src="Winter-Grass.jpg" alt="">'*/

      var containerPostsParent = document.createElement("div");
      containerPostsParent.className = "col-xs-12 col-sm-12 col-md-9 col-lg-9 posts";

      var containerPosts = document.createElement("div");
      containerPosts.className = "col-xs-12 col-sm-12 col-md-12 col-lg-12";

      var treeOfPostsParent = document.createElement("div");
      treeOfPostsParent.className = "hidden-xs hidden-sm col-md-3 col-lg-3 tree";

      var treeOfPosts = document.createElement("div");
      treeOfPosts.className = "col-md-12 col-lg-12";

      var treePostList = document.createElement('ul');

      mainContainer.appendChild(jumbotron);

      containerPostsParent.appendChild(containerPosts);
      treeOfPostsParent.appendChild(treeOfPosts);
      mainContainer.appendChild(containerPostsParent);
      mainContainer.appendChild(treeOfPostsParent);
 
      for (var i = 0; i < answer[0].length; i++) {

        var link = document.createElement('a');
        link.href = "#/blog/" + answer[0][i].ID + "/" + answer[0][i].post_name;
        link.name = "thumbnaillink";

        var url = document.createElement('a');
        url.href = "#/blog/" + answer[0][i].ID + "/" + answer[0][i].post_name;
        
        var thumbnail = document.createElement('article');
        thumbnail.id = "thumbnail" + i;
        thumbnail.className = "thumbnail";

        var dateTime = answer[0][i].post_date_gmt.split(" ");
        var date = dateTime[0].split("-");
        var time = dateTime[1].split(":");
        var dateView = date[2] + "/" + date[1] + "/" + date[0];
        var timeView = time[0] + ":" + time[1];

        thumbnail.innerHTML = "<header><h3>" + answer[0][i].post_title + "</h3>" + "<h6>Posted by <span>" + answer[1][i] + "</span> on " + dateView + " at " + timeView + "</h6><p>" + answer[2][i] + "</p></header><div id=" + ("post" + answer[0][i].ID) + "></div>";

        link.appendChild(thumbnail);
        containerPosts.appendChild(link);

        var postLi = document.createElement('li');

        postLi.innerHTML = "<span class='btn' id=" + 'toggleThumbnail' + i + ">" + dateView + " at " + timeView + "\r\n" + answer[0][i].post_title + "</span>";
        //postLi.addEventListener('click', function() { postTreeToggle("thumbnail" + i) });

        url.appendChild(postLi);

        treePostList.appendChild(url);

        postTreeToggle("#toggleThumbnail" + i, "#thumbnail" + i); 
        toggleThumbnail("#thumbnail" + i, "#post" + answer[0][i].ID);
      }

      treeOfPosts.appendChild(treePostList);

    }
  }
     
  xhttp.open("GET", "<?php echo get_site_url(); ?>/wp-json/posts/title", true);
  xhttp.send();
  
}

function postTreeToggle(togglerID, thumbnailID) {
  $(document).ready(function(){
    $(togglerID).click(
      function(){
      $(thumbnailID).click(function() { scrollToPost(thumbnailID) });
    });
  });
}

//basic scroll function, doesn't work now
function scrollToPost(post) {
  $(document).ready(function(){
    $(thumbnailId).click(
      function(){
      $(postId).animate({height: "toggle", opacity: "toggle"}, {duration: "slow"});
    });
  });
  element = post;
  alignWithTop = true;
  element.scrollIntoView(alignWithTop);
  window.scrollBy(0, -50);
}

function getPostContent(link) {
  console.log(link);

  if (document.getElementsByName("thumbnaillink").length != 0) {
    ajaxRequest(link);
    console.log('"thumbnaillink".length != 0');
  } else if (document.getElementsByName("thumbnaillink").length == 0) {  
    getPosts();
    ajaxRequest(link);
    console.log('"thumbnaillink".length == 0');
    } 

} 

  
function ajaxRequest(link) {

  var thumbnailContent, l;
  l = link.split("/");

  if (localStorage.getItem(link) == null) {
  
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var answer = JSON.parse(xhttp.responseText);

       var thumbnailContent = document.getElementById("post" + l[0]); 

        if (answer[0] == "null") {
          thumbnailContent.innerHTML = "No content!";
          console.log("ajaxRequest if if", "answer == ", answer[0]);
        } else {
          thumbnailContent.innerHTML = answer[0];
          localStorage.setItem(link, answer[0]);
          console.log("ajaxRequest if else", "answer == ", answer[0]);
        }
      }
    }

    xhttp.open("GET", "<?php echo get_site_url(); ?>/wp-json/posts/content/?id=" + l[0] + "&post_name=" + l[1], true);
    xhttp.send();

  } else {
    thumbnailContent = document.getElementById("post" + l[0]); 
    thumbnailContent.innerHTML = localStorage.getItem(link);
    console.log("ajaxRequest else", "localStorage == ", localStorage.getItem(link));
  }
}


function toggleThumbnail(thumbnailId, postId) {
  $(postId).hide();
  $(document).ready(function(){
    $(thumbnailId).click(
      function(){
      $(postId).animate({height: "toggle", opacity: "toggle"}, {duration: "slow"});
    });
  });
}

//albums page
function setSizeOfTitle() {
  var thumbnailSizeDivided = document.getElementById('thumbnail').offsetWidth / 10;
  var title = document.getElementById('title');
  console.log("New size of titles in thumbnails: " + thumbnailSizeDivided);
  title.style.fontSize = thumbnailSizeDivided + "px";
}

function setImageSize() {
  if(document.getElementById("cover") != null) {
    var colSize = window.getComputedStyle(document.getElementById("cover"), null).getPropertyValue("width");
  } else if(document.getElementById("artist") != null) {
    var colSize = window.getComputedStyle(document.getElementById("artist"), null).getPropertyValue("width");
  }
  
  var imgSize = colSize.split("px")[0] - 30;
  var img = document.getElementById("image");


  img.width = imgSize;
  img.height = imgSize;
}

function albumList() {
  var xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var answer = JSON.parse(this.responseText);
      console.log(answer);

      var container = document.getElementById('content');

      for(var i=0; i < answer.length; i++) {
        var url = document.createElement('a');
        url.id = decodeURIComponent(answer[i].Artist_tr) + "_" + decodeURIComponent(answer[i].Album_tr);
        url.href = "#/albums/" + decodeURIComponent(answer[i].Artist_tr) + "/" + decodeURIComponent(answer[i].Album_tr);
        console.log(url.href);

        var albumContainerParent = document.createElement('div');
        albumContainerParent.className = "col-xs-12 col-sm-4 col-md-4 col-lg-4";

        var albumContainer = document.createElement('div');
        albumContainer.className = "col-xs-12 col-sm-12 col-md-12 col-lg-12";

        var albumThumbnail = document.createElement('div');
        albumThumbnail.className = "thumbnail col-xs-12 col-sm-12 col-md-12 col-lg-12 album-img";
        albumThumbnail.id = "thumbnail";

        albumThumbnail.innerHTML = '<img src="' + answer[i].image + '" alt="' + answer[i].image + '">' + '<figcaption class="tilter__caption"><h3 class="tilter__title" id="title">' + decodeURIComponent(answer[i].Artist) + '</h3>' + '<p class="tilter__description" id="title">' + decodeURIComponent(answer[i].Album) + '</p></figcaption>';

        albumContainer.appendChild(albumThumbnail);
        url.appendChild(albumContainer);
        albumContainerParent.appendChild(url);
        container.appendChild(albumContainerParent);

        addClicker(document.getElementById(url.id));
      }
    }
    window.onresize = function() {setSizeOfTitle()};
  };

  xhttp.open("GET", "<?php echo get_site_url(); ?>/wp-json/album/title/", true);
  xhttp.send();
} 


function getAlbumContent(link) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var answer = JSON.parse(this.responseText);
      console.log(answer);

      var content = document.createElement("div");
      content.className = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';

      var buyLinks = document.createElement("div");
      buyLinks.className = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';

      var albumImage = document.createElement("div");
      albumImage.className = 'col-xs-6 col-sm-6 col-md-6 col-lg-6';
      albumImage.id = 'cover';

      var trackListContainer = document.createElement("div");
      trackListContainer.className = 'col-xs-6 col-sm-6 col-md-6 col-lg-6';

      var trackList = document.createElement("table");
      trackList.id = 'tracks';
      trackListContainer.appendChild(trackList);

      var annotationField = document.createElement("div");
      annotationField.className = 'col-xs-12 col-sm-12 col-md-6 col-lg-6';
      annotationField.id = 'annotation-album';

      var albumSourcesList = [];

      for (var i = 0; i < answer[1].length; i++) {

        var newRow = trackList.insertRow(i);
        var cellF = newRow.insertCell(0);
        var cellS = newRow.insertCell(1);
        var cellT = newRow.insertCell(2);

        var trackOrder = document.createElement('p');
        trackOrder.innerHTML = i + 1 + '. ';

        /*var audio = document.createElement('audio');

        audio.className = 'track';
        audio.id = i;

        audio.src = answer[1][i].FileDir;
        audio.setAttribute('controls', true);*/

        albumSourcesList.push(answer[1][i].FileDir);

        var trackButton = document.createElement('div');
        trackButton.innerHTML = '<button class="glyphicon glyphicon-play" id="' + answer[1][i].FileDir + '" onclick="playTrack(' + "'" + answer[1][i].FileDir + "'" + ')"></button>';

        var trackName = document.createElement('p');

        trackName.innerHTML = decodeURIComponent(answer[1][i].Track);

        cellF.appendChild(trackOrder);
        cellS.appendChild(trackButton);
        cellT.appendChild(trackName);

      }

      if(answer[0][0].itunes != "") {
        var itunesBuyLink = document.createElement('div');
        itunesBuyLink.innerHTML = '<a href="' + answer[0][0].itunes + '"><img src="<?php echo get_theme_root_uri() . '/aeris/getonitunes.svg'?>">';
        buyLinks.appendChild(itunesBuyLink);
      }

      if(answer[0][0].googleplay != "") {
        var googleplayBuyLink = document.createElement('div');
        googleplayBuyLink.innerHTML = '<a href="' + answer[0][0].googleplay + '"><img src="<?php echo get_theme_root_uri() . '/aeris/getongoogleplay.svg'?>">';
        buyLinks.appendChild(googleplayBuyLink);
      }

      content.appendChild(buyLinks);
      content.appendChild(albumImage);
      content.appendChild(trackListContainer);
      content.appendChild(annotationField);

      var container = document.getElementById("content"); 
      container.appendChild(content);
      
      var img = document.createElement("img");
      img.id = 'image';
      img.src = answer[0][0].image;

      albumImage.appendChild(img);

      var textfield = document.getElementById("annotation-album");
      textfield.innerHTML = decodeURIComponent(answer[0][0].Annotation);  

      checkOnplay();
      setImageSize();

      if(albumSourcesList != audioSourcesList) {
        audioSourcesList = [];
        audioSourcesList = albumSourcesList;
      }
    };
  }
  var artist_album = link.split('/');
  var artist = artist_album[0];
  var album = artist_album[1];
  console.log(artist, album);
  xhttp.open("GET", "<?php echo get_site_url(); ?>/wp-json/album/tracks/?artist_tr=" + artist + "&album_tr=" + album, true);
  xhttp.send();
} 

function checkOnplay() {
  document.addEventListener("play", function(e){ 

    var tracks = document.getElementsByClassName('track');

    for (var i = 0; i < tracks.length; i++) {

      if(tracks[i] != e.target) {

        tracks[i].pause();

        console.log(e.target.id + ' is playing')
      }
    }
  }, true);
}

//artists page
function artistList() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var answer = JSON.parse(this.responseText);

      var container = document.getElementById('content');

      for(var i=0; i < answer.length; i++) {
        var url = document.createElement('a');
        url.id = decodeURIComponent(answer[i].Name_tr);
        url.href = "#/artists/" + decodeURIComponent(answer[i].Name_tr);

        var artistContainerParent = document.createElement('div');
        artistContainerParent.className = "col-xs-12 col-sm-4 col-md-4 col-lg-4";

        var artistContainer = document.createElement('div');
        artistContainer.className = "col-xs-12 col-sm-12 col-md-12 col-lg-12";

        var artistThumbnail = document.createElement('div');
        artistThumbnail.className = "thumbnail col-xs-12 col-sm-12 col-md-12 col-lg-12";
        artistThumbnail.id = "artist";
        
        artistThumbnail.innerHTML = '<img src="' + answer[i].image + '" width="320" height="320" alt="' + answer[i].image + '">' + '<figcaption class="tilter__caption"><h3 class="tilter__title" id="title">' + decodeURIComponent(answer[i].Name) + '</h3></figcaption>';

        artistContainer.appendChild(artistThumbnail);
        url.appendChild(artistContainer);
        artistContainerParent.appendChild(url);
        container.appendChild(artistContainerParent);  

        addClicker(document.getElementById(url.id));   
      }
    }
  window.onresize = function() {setSizeOfTitle()};
  };
  xhttp.open("GET", "<?php echo get_site_url(); ?>/wp-json/artists/view/", true);
  xhttp.send();
} 

function loadArtistPage(link) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var answer = JSON.parse(this.responseText);

      var content = document.createElement("div");
      content.className = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';

      var artistImage = document.createElement("div");
      artistImage.className = 'hidden-xs col-sm-2 col-md-2 col-lg-2';
      artistImage.id = 'artist';

      var annotationField = document.createElement("div");
      annotationField.className = 'col-xs-12 col-sm-10 col-md-10 col-lg-10';
      annotationField.id = 'annotation-artist';

      content.appendChild(artistImage);
      content.appendChild(annotationField);

      var container = document.getElementById("content"); 
      container.appendChild(content);
      
      var img = document.createElement("img");
      img.id = 'image';
      img.src = answer[0].image;
      artistImage.appendChild(img);

      var textfield = document.getElementById("annotation-artist");
      textfield.innerHTML = decodeURIComponent(answer[0].Annotation);  

      setImageSize();
      }
  };
  var artist = link;
  console.log(artist);
  xhttp.open("GET", "<?php echo get_site_url(); ?>/wp-json/artists/annotation/?name_tr=" + artist, true);
  xhttp.send();
} 

</script>

<?php get_footer(); ?>