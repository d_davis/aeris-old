<?php

function modify_jquery_version() {
    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery',
'https://code.jquery.com/jquery-3.2.1.min.js', false, '3.2.1');
        wp_enqueue_script('jquery');
    }
}
add_action('init', 'modify_jquery_version');


add_action( 'rest_api_init', function () {
        register_rest_route( 'album', '/title/', array(
                'methods' => 'GET',
                'callback' => 'album_title'
        ));
});

function album_title() {

	global $wpdb;
	$album_list = $wpdb->get_results("SELECT Artist, Album, Artist_tr, Album_tr, image FROM mngr_albums ORDER BY Releasedate", ARRAY_A);

	return $album_list;
}


add_action( 'rest_api_init', function () {
        register_rest_route( 'album', '/tracks/', array(
                'methods' => 'GET',
                'callback' => 'album_tracks'
        ));
});

function album_tracks(\WP_REST_Request $request) {
	$artist_tr = $request['artist_tr'];
	$album_tr = $request['album_tr'];

	global $wpdb;
	$album_info = $wpdb->get_results("SELECT Artist, Album, image, Releasedate, Genre, Annotation, itunes, googleplay FROM mngr_albums WHERE Artist_tr = '".$artist_tr."' AND Album_tr = '".$album_tr."'");
	$tracks = $wpdb->get_results("SELECT FileDir, Track FROM mngr_tracks WHERE Artist_tr = '".$artist_tr."' AND Album_tr = '".$album_tr."'");
	
	return array($album_info, $tracks);
}

add_action( 'rest_api_init', function () {
        register_rest_route( 'artists', '/view/', array(
                'methods' => 'GET',
                'callback' => 'artists_view'
        ));
});

function artists_view(\WP_REST_Request $request) {

	global $wpdb;
	$artists = $wpdb->get_results("SELECT Name, Name_tr, image FROM mngr_artists");
	
	return $artists;
}

add_action( 'rest_api_init', function () {
        register_rest_route( 'artists', '/annotation/', array(
                'methods' => 'GET',
                'callback' => 'artists_annotation'
        ));
});

function artists_annotation(\WP_REST_Request $request) {
	$name_tr = $request['name_tr'];

	global $wpdb;
	$annotation = $wpdb->get_results("SELECT * FROM mngr_artists WHERE Name_tr='".$name_tr."'");
	
	return $annotation;
}

add_action( 'rest_api_init', function () {
        register_rest_route( 'posts', '/title/', array(
                'methods' => 'GET',
                'callback' => 'get_post_title'
        ));
});

function get_post_title(\WP_REST_Request $request) {

	global $wpdb;
	$post_title = $wpdb->get_results("SELECT ID, post_title, post_date_gmt, post_name FROM wp_posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY ID DESC");

	$post_author = $wpdb->get_results("SELECT post_author FROM wp_posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY ID DESC");

	$authors = [];

	foreach($post_author as $p) {
		$get_author_from_user_table = $wpdb->get_results("SELECT user_nicename FROM wp_users WHERE ID='".$p->post_author."'");
		$authors[] = $get_author_from_user_table[0]->{"user_nicename"};
	}

	$post_content_json = $wpdb->get_results("SELECT ID, post_content FROM wp_posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY ID DESC");

	$posts_description = [];

	foreach($post_content_json as $p) {
		$post_content_exploded = explode("<!--more-->", $p->post_content);
		$posts_description[] = $post_content_exploded[0];
	}

	return array($post_title, $authors, $posts_description);

}

add_action( 'rest_api_init', function () {
        register_rest_route( 'posts', '/content/', array(
                'methods' => 'GET',
                'callback' => 'get_post_content'
        ));
});


function get_post_content(\WP_REST_Request $request) {
	$id = $request['id']; //1 (int)
	$post_title = $request['post_name']; //Hello world! (string)
	//$part = $request['part']; //0 (int 0 or 1)

	global $wpdb;
	$post_content_json = $wpdb->get_results("SELECT post_content FROM wp_posts WHERE ID = '".$id."' AND post_name = '".$post_title."'");

	$post_content_exploded = explode("<!--more-->", $post_content_json[0]->{"post_content"});

	$post_content_after_tag = $post_content_exploded[1];

	/*if ($part == 0) {
		return $post_content_before_tag;
	} else if ($part == 1) {
		return $post_content_after_tag;
	} else {
		return "Request error";
	}*/
	return array($post_content_after_tag);
}

add_action( 'rest_api_init', function () {
        register_rest_route( 'audio', '/stream/', array(
                'methods' => 'GET',
                'callback' => 'request_file'
        ));
});

function request_file(\WP_REST_Request $request) {

	$dir = str_replace("\\", "/", WP_CONTENT_DIR);
	$filepath = $dir."/"."s.mp3";
	$file = fopen($filepath, "r");
	$filesize = filesize($filepath);
	$read = fread($file, $filesize);
	fclose($file);

	echo $read;

	//$wanted_data = $request['wanted_data'];


	//$data = fgets($file, 4096); //getDataFromFile($file, $DATA_LENGTH-$wanted_data, 8128);

	/*$extension = "mp3";
	$mime_type = "audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3";

	
	    header('Content-type: {$mime_type}');
	    header('Content-length: ' . filesize($file));
	    header("Content-Transfer-Encoding: chunked"); 
	    header('Content-Disposition: filename="' . $filename);
	    header('X-Pad: avoid browser bug');
	    header('Cache-Control: no-cache');*/
	
	
	//}else{
	    //header("HTTP/1.0 404 Not Found");
	//}
}

function getDataFromFile($fileName, $start, $length) {
    $f_handle = fopen($filename, 'r');
    $meta = stream_get_meta_data($f);
    fseek($f_handle, $start);
    $str = fgets($f_handle, $length);
    fclose($f_handle);
    return array($meta['mode'], $str);
}

add_action( 'rest_api_init', function () {
        register_rest_route( 'audio', '/content_range/', array(
                'methods' => 'GET',
                'callback' => 'content_range'
        ));
});

function content_range() {
	// Clears the cache and prevent unwanted output
	ob_clean();
	@ini_set('error_reporting', E_ALL & ~ E_NOTICE);
	@apache_setenv('no-gzip', 1);
	@ini_set('zlib.output_compression', 'Off');

	$file = "C://Users//user//Desktop//25.mp3"; // The media file's location
	$mime = "audio/mpeg"; // The MIME type of the file, this should be replaced with your own.
	$size = filesize($file); // The size of the file

	// Send the content type header
	header('Content-type: ' . $mime);

	// Check if it's a HTTP range request
	if(isset($_SERVER['HTTP_RANGE'])){
	    // Parse the range header to get the byte offset
	    $ranges = array_map(
	        'intval', // Parse the parts into integer
	        explode(
	            '-', // The range separator
	            substr($_SERVER['HTTP_RANGE'], 6) // Skip the `bytes=` part of the header
	        )
	    );

	    // If the last range param is empty, it means the EOF (End of File)
	    if(!$ranges[1]){
	        $ranges[1] = $size - 1;
	    }

	    // Send the appropriate headers
	    header('HTTP/1.1 206 Partial Content');
	    header('Accept-Ranges: bytes');
	    header('Content-Length: ' . ($ranges[1] - $ranges[0])); // The size of the range

	    // Send the ranges we offered
	    header(
	        sprintf(
	            'Content-Range: bytes %d-%d/%d', // The header format
	            $ranges[0], // The start range
	            $ranges[1], // The end range
	            $size // Total size of the file
	        )
	    );

	    // It's time to output the file
	    $f = fopen($file, 'rb'); // Open the file in binary mode
	    $chunkSize = 8192; // The size of each chunk to output

	    // Seek to the requested start range
	    fseek($f, $ranges[0]);

	    // Start outputting the data
	    while(true){
	        // Check if we have outputted all the data requested
	        if(ftell($f) >= $ranges[1]){
	            break;
	        }

	        // Output the data
	        echo fread($f, $chunkSize);

	        // Flush the buffer immediately
	        @ob_flush();
	        flush();
	    }
	}
	else {
	    // It's not a range request, output the file anyway
	    header('Content-Length: ' . $size);

	    // Read the file
	    @readfile($file);

	    // and flush the buffer
	    @ob_flush();
	    flush();
	}
}


?>