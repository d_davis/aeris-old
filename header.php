<!doctype html>
<html>
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Aeris</title>
  <?php
  wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
  wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
  wp_enqueue_script('p5', 'https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.5.16/p5.min.js');
  wp_enqueue_script('p5dom', 'https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.5.16/addons/p5.dom.min.js');
  wp_enqueue_script('p5sound', 'https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.5.16/addons/p5.sound.min.js');
  //wp_enqueue_script('processing', get_theme_root_uri() . '/aeris/processing.min.js');
  //wp_enqueue_script('brownian', get_theme_root_uri() . '/aeris/brownian.js');
  wp_enqueue_script('triangulation', get_theme_root_uri() . '/aeris/triangles.js');
  wp_enqueue_style('evolventa', get_theme_root_uri() . '/aeris/evolventa/evolventa.css');
  wp_enqueue_style('style', get_theme_root_uri() . '/aeris/style.css');
  //wp_enqueue_script('howler', 'https://cdnjs.cloudflare.com/ajax/libs/howler/2.0.4/howler.js');

  wp_head();
  ?>

</head>

<body>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<div id='brownian'></div>
<!--<div id="player">inner content</div>-->

<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
<div class="container-fluid">

    <div class="navbar-header page-scroll">

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topNavBar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a id="blog" class="navbar-brand font-smoothing" href="#/blog">Aeris</a>
    </div>

    <div class="collapse navbar-collapse" id="topNavBar">

      <ul class="nav navbar-nav navbar-right">

        <li class="font-smoothing nav-tabs">
          <a id="albums" href="#/albums">ALBUMS</a>
        </li>

        <li class="font-smoothing nav-tabs">
          <a id="artists" href="#/artists">ARTISTS</a>
        </li>

      </ul>
    </div>

</div>
</nav>